package com.mjolnirr.maven.goals;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 29.10.13
 * Time: 12:18
 * To change this template use File | Settings | File Templates.
 */
@XStreamAlias("method")
public class ManifestMethod {
    @XStreamAsAttribute
    private String name;

    @XStreamAsAttribute
    @XStreamAlias("type")
    private String returnType;

    private List<ManifestParameter> parameters;

    public ManifestMethod(String name, String returnType, List<ManifestParameter> parameters) {
        this.name = name;
        this.returnType = returnType;
        this.parameters = parameters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public List<ManifestParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<ManifestParameter> parameters) {
        this.parameters = parameters;
    }
}
