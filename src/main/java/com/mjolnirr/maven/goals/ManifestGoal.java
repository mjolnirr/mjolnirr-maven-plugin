package com.mjolnirr.maven.goals;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.mjolnirr.lib.component.AbstractApplication;
import com.mjolnirr.lib.component.AbstractModule;
import com.mjolnirr.lib.component.ComponentContext;
import com.thoughtworks.xstream.XStream;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Goal which preprocesses the main class to generate manifest
 */
@Mojo( name = "manifest", defaultPhase = LifecyclePhase.PROCESS_CLASSES )
public class ManifestGoal extends AbstractMojo {
    /**
     * @parameter expression="${project}"
     * @required
     * @readonly
     */
    @Parameter(defaultValue = "${project}")
    private MavenProject project;
    /**
     * Location of the file.
     */
    @Parameter(property = "mainClass", required = true)
    private String mainClass;

    public void execute() throws MojoExecutionException {
        Log logger = getLog();

        logger.info("Processing " + mainClass);

        try {
            Class bundle = getClassloader().loadClass(mainClass);
            int type;

            if (AbstractModule.class.isAssignableFrom(bundle)) {
                //  This is module
                type = Manifest.TYPE_MODULE;
            } else if (AbstractApplication.class.isAssignableFrom(bundle)) {
                //  This is application
                type = Manifest.TYPE_APPLICATION;
            } else {
                throw new MojoExecutionException("Your main class doesn't extend neither com.mjolnirr.lib.component.AbstractModule nor com.mjolnirr.lib.component.AbstractApplication!");
            }

            List<ManifestMethod> methods = new ArrayList<ManifestMethod>();

            for (Method method : bundle.getMethods()) {
                //  If this method is inherited from the Object class we don't need it
                if (isObjectMethod(method) || isComponentInitialize(method)) {
                    continue;
                }

                List<ManifestParameter> parameters = new ArrayList<ManifestParameter>();

                for (Class param : method.getParameterTypes()) {
                    parameters.add(new ManifestParameter(param.getCanonicalName()));
                }

                methods.add(new ManifestMethod(method.getName(), method.getReturnType().getCanonicalName(), parameters));
            }

            Manifest manifest = new Manifest(type, bundle.getSimpleName(), bundle.getCanonicalName(), methods);

            XStream xstream = new XStream();

            xstream.aliasField(manifest.getType(), Envelope.class, "manifest");
            xstream.processAnnotations(Envelope.class);
            xstream.processAnnotations(Manifest.class);
            xstream.processAnnotations(ManifestMethod.class);
            xstream.processAnnotations(ManifestParameter.class);

            xstream.toXML(new Envelope(manifest), new FileOutputStream(new File(project.getBasedir(), "target/classes/manifest.xml")));
        } catch (ClassNotFoundException e) {
            throw new MojoExecutionException("Main class not found!", e);
        } catch (MalformedURLException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        } catch (DependencyResolutionRequiredException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        } catch (FileNotFoundException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    private boolean isComponentInitialize(Method method) {
        if (!method.getName().equals("initialize")) {
            return false;
        }

        Class[] params = method.getParameterTypes();

        if (params.length != 1) {
            return false;
        }

        return (params[0].getCanonicalName().equals(ComponentContext.class.getCanonicalName()));
    }

    /**
     *  Return true if method inherited from the Object
     * */
    private boolean isObjectMethod(Method method) {
        return method.getDeclaringClass().getCanonicalName().equals(Object.class.getCanonicalName());
    }

    private URLClassLoader getClassloader() throws MalformedURLException, DependencyResolutionRequiredException {
        List runtimeClasspathElements = project.getRuntimeClasspathElements();
        URL[] runtimeUrls = new URL[runtimeClasspathElements.size()];
        for (int i = 0; i < runtimeClasspathElements.size(); i++) {
            String element = (String) runtimeClasspathElements.get(i);
            runtimeUrls[i] = new File(element).toURI().toURL();
        }

        return new URLClassLoader(runtimeUrls, Thread.currentThread().getContextClassLoader());
    }
}
