package com.mjolnirr.maven.goals;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 29.10.13
 * Time: 12:19
 * To change this template use File | Settings | File Templates.
 */
@XStreamAlias("parameter")
public class ManifestParameter {
    @XStreamAsAttribute
    private String type;

    public ManifestParameter(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
