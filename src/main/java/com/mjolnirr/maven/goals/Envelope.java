package com.mjolnirr.maven.goals;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 29.10.13
 * Time: 11:29
 * To change this template use File | Settings | File Templates.
 */
@XStreamAlias("hive")
public class Envelope {
    private Manifest manifest;

    public Envelope(Manifest manifest) {
        this.manifest = manifest;
    }
}
