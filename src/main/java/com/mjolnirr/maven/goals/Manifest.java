package com.mjolnirr.maven.goals;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import java.util.List;

public class Manifest {
    public static final int TYPE_MODULE = 0;
    public static final int TYPE_APPLICATION = 1;

    @XStreamOmitField
    private int type;

    @XStreamAsAttribute
    private String name;

    @XStreamAsAttribute
    private String classname;

    private List<ManifestMethod> methods;

    public Manifest(int type, String name, String classname, List<ManifestMethod> methods) {
        this.type = type;
        this.name = name;
        this.classname = classname;
        this.methods = methods;
    }

    public String getType() {
        if (type == Manifest.TYPE_APPLICATION) {
            return "application";
        } else {
            return "module";
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public List<ManifestMethod> getMethods() {
        return methods;
    }

    public void setMethods(List<ManifestMethod> methods) {
        this.methods = methods;
    }
}
